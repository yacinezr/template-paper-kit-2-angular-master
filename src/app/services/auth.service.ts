import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

import { auth } from "firebase/app";
import { AngularFireAuth } from "@angular/fire/auth";
import {
  AngularFirestore,
  AngularFirestoreDocument
} from "@angular/fire/firestore";

import { Observable, of } from "rxjs";
import { switchMap } from "rxjs/operators";
import { User } from "./user.model";
// https://github.com/firebase/quickstart-js/blob/master/auth/google-redirect.html !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

@Injectable({ providedIn: "root" })
export class AuthService {
  user$: Observable<any>;

  constructor(
    private angularFireAuth: AngularFireAuth,
    private angularFirestore: AngularFirestore,
    private router: Router
  ) {
    this.user$ = this.angularFireAuth.authState.pipe(
      
      switchMap(user => {
        console.log("user", user);
        if (user) {
          return this.angularFirestore
            .doc<User>(`users/${user.providerData[0].email}`)
            .valueChanges();
        } else {
          return of(null);
        }
      })
    );
  }

  async googleSignin() {
    try {
      const provider = new auth.GoogleAuthProvider();
      provider.addScope('email');
      const credential = await this.angularFireAuth.auth.signInWithPopup(provider);
      return this.updateUserData(credential);
    } catch (err) {
      console.error(err);
    }
  }

  async facebookSignin() {
    try {
      const provider = new auth.FacebookAuthProvider();
      provider.addScope('email');
      const credential = await this.angularFireAuth.auth.signInWithPopup(provider);
      return this.updateUserData(credential);
    } catch (err) {
      console.error(err);
    }
  }

  async signOut() {
    await this.angularFireAuth.auth.signOut();
    return this.router.navigate(["/"]);
  }

  private updateUserData(credential) {
    // Sets user data to firestore on login
    console.log("credential   ",credential);
    const userRefByUID: AngularFirestoreDocument<User> = this.angularFirestore.doc(`users/${credential.user.uid}`);
    const userRefByEmail: AngularFirestoreDocument<User> = this.angularFirestore.doc(`users/${credential.additionalUserInfo.profile.email}`);

    console.log("userRefByUID", userRefByUID)
    console.log("userRefByEmail", userRefByEmail)

    const data = {
      uid: credential.user.uid,
      email: credential.additionalUserInfo.profile.email,
      displayName: credential.user.displayName,
      photoURL: credential.user.photoURL
    };

    return userRefByEmail.set(data, { merge: true });
  }
}
